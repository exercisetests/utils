#include <iostream>

int main(int argc, char *argv[])
{
    int numOfLines = 50;

    if(argc == 2)
        numOfLines = atoi(argv[1]);

    for(unsigned int i = 0; i < numOfLines; i++)
        std::cout << " \n" << std::endl;
        
    return 0;
}