#!/bin/sh

# Check if the right number of arguments was provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Extract the directory argument
directory="$1"

# Check if the directory exists
if [ ! -d "$directory" ]; then
    echo "Error: The directory '$directory' does not exist."
    exit 1
fi

# Determine the directory where the script is located
script_dir="$(dirname "$0")"

# Check if cppLineFinder.py is executable
if [ ! -x "$script_dir/cppLineFinder.py" ]; then
    echo "Error: cppLineFinder.py is not executable."
    exit 1
fi

# Run the cppLineFinder.py script with the specified arguments
"$script_dir/cppLineFinder.py" "$directory" --all

# Check if the command was successful
if [ $? -ne 0 ]; then
    echo "Error: The command failed."
    exit 1
fi

echo "Command executed successfully."
