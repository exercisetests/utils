#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
using std::cout;
using std::endl;

#define EX_CODES_FILE "CodesToMessages.csv"
#define ERROR_MESSAGE_CODE_NOT_FOUND "Error - message code not found"

std::string readFileToString(const std::string fileName)
{
	std::ifstream inFile;
	inFile.open(fileName); //open the input file

	std::stringstream strStream;
	strStream << inFile.rdbuf(); //read the file
	std::string str = strStream.str(); //str holds the content of the file

	return str;
}

void clearCommaFromEndOfString(std::string& str)
{
	if(str[str.length()  - 2] == ',')
		str.erase(str.length() - 2);
}

void initializeMap(std::map<std::string, std::string>& codeToMessage)
{
	std::string fileContent = readFileToString(EX_CODES_FILE);
	std::string element;
	std::string codeString;
	std::string message;
	const std::string line_delimiter_char = ".\n";
	const std::string values_delimiter_char = ",";
	size_t pos1 = 0;
	size_t pos2 = 0;
	size_t pos3 = 0;
	int messageCode;

	// prestep remove csv headers
	pos1 = fileContent.find("\n");
	fileContent.erase(0, pos1 + 1);

	while ((pos1 = fileContent.find(line_delimiter_char)) != std::string::npos)
	{
		element = fileContent.substr(0, pos1);
		pos2 = fileContent.find(values_delimiter_char);
		codeString = fileContent.substr(0, pos2);
		pos3 = fileContent.find_first_of('.');
		message = fileContent.substr(pos2 + 1, pos3 - pos2);
		clearCommaFromEndOfString(message);
		codeToMessage.insert(std::pair<std::string, std::string>(codeString, message));
		fileContent.erase(0, pos1 + line_delimiter_char.length());
	}
}

std::string getMessage(const std::string messageCodeString, const std::map<std::string, std::string>& codeToMessage)
{
	auto it = codeToMessage.find(messageCodeString);
	if (it == codeToMessage.end())
	{
		return ERROR_MESSAGE_CODE_NOT_FOUND;
	}
	return it->second;
}


int main(int argc, char* argv[])
{
	// check num of arguments
	if (argc < 2 || argc > 3)
	{
		std::cerr <<
			"Wrong number of main arguments\n" <<
			"Expected 1 argument - result code\n" <<
			"Usage example: ./Ex1Part1Test 01121" << std::endl;
		exit(3);
	}


	std::map<std::string, std::string> codeToMessage;
	initializeMap(codeToMessage);
	std::string messageCodeString = std::string(argv[1]);
	std::string message = getMessage(messageCodeString, codeToMessage);
	std::cout << message << std::endl;
	if (message == ERROR_MESSAGE_CODE_NOT_FOUND)
		return 1;
	return 0;
}